/**
*
* Dit is voor alle videos af te halen ven verschillende clients (youtube/vimeo/...)
* Staat in een aparte file omdat dit ook gebruikt wordt voor de Chrome extentie
* Hierdoor worden aanpassingen ineens naar bookmarklet en extentie gepushed
*
**/

function getVideos(client){
	//variables
	var url = document.URL;
	var videoArr = new Array();

	//switch voor de client (host/provider/..) als er een nieuwe client supported is moet er gewoon een nieuwe case toegevoegt worden
	switch(client){
		/* DEFAULT (alles ophalen van iframes enzo) */
		default:
			//als er iframes gevonden zijn
			if($("iframe").length !=0) {
				//src van iframe ophalen en het woord youtube of vimeo er uit halen
				var src = $('iframe').attr('src');
				//if youtube
				if(src.indexOf('youtube') >= 0){
					//video ID uit src halen
					var splitUrl = src.split('/');
					src = splitUrl[splitUrl.length-1];

					var obj = {};
					obj.id = src;
					obj.img = 'http://i4.ytimg.com/vi/' + src + '/mqdefault.jpg';
					obj.title = 'Title not found';
					obj.client = 'y';
					obj.author = 'Unknown';
					videoArr.push(obj);
				}
				//if vimeo
				else if(src.indexOf('vimeo') >= 0){
					//video ID uit src halen
					var splitUrl = src.split('/');
					src = splitUrl[splitUrl.length-1];

					var obj = {};
					obj.id = src.replace(/\D+/, '');;
					obj.img = '//clients.contentcowboys.eu/mychannls/img/channl.png';
					obj.title = 'Title not found';
					obj.client = 'v';
					obj.author = 'Unknown';
					videoArr.push(obj);
				}
			}
		break;
		/* DEFAULT END */

		/* YOUTUBE */
		case 'y':
			
			//video ID uit url halen
			splitUrl = url.split('v=');
			if(splitUrl[1] !== undefined){
				url = splitUrl[1].split('&');
				var cleanUrl = url[0].replace(/\b\#+/g, '');
				cleanUrl = cleanUrl.replace(/\b\!+/g, '');
			}

			if(cleanUrl){
				var apiUrl = "http://gdata.youtube.com/feeds/api/videos?q=" + cleanUrl + "&alt=json";
				$.getJSON(apiUrl, function(data) {
				 	var items = [];

				  	result = data.feed.entry;

				  	//video object toevoegen aan video array
					var obj = {};
					var id = (result[0].id.$t).split('/');
					id = id[id.length-1];
					obj.id = id;
					obj.img = 'http://i4.ytimg.com/vi/' + id + '/mqdefault.jpg';
					obj.title = result[0].title.$t;
					obj.client = 'y';
					obj.author = result[0].author[0].name.$t;
					videoArr.push(obj);

					videosLoaded();
				});
			} else {
				var youtubeArr = new Array();
				var youtubeUnique = new Array();

				//elke a tag opzoeken en href opvragen
				$('a').each(function(){
					var link = $(this).attr('href')

					var patt1=new RegExp("watch");
					//checken of er watch in de url staat
					if(patt1.test(link)){

						//video paramater opzoeken (videoID)
						var link=link.replace("/watch","");
						var params = getQueryParams(link);
						if(params.v !== undefined){
							youtubeArr.push(params.v);
						}
					}
				});

				//duplicates deleten
				$.each(youtubeArr, function(i, el){
				    if($.inArray(el, youtubeUnique) === -1) youtubeUnique.push(el);
				});

				//per 10 rekenen
				var apiStr = "";
				var strArr = new Array();
				var strCount = 0;

				for(var i=0;i<youtubeUnique.length;i++) {
					if(youtubeUnique[i].indexOf("-") == -1){
						apiStr += youtubeUnique[i] +'|';
						strCount ++;
					} else {
						if(apiStr != ''){
							strArr.push(apiStr);
						}
						strArr.push(youtubeUnique[i]);
						apiStr = '';
						strCount = 1;
					}
					if(strCount % 10 === 0 && i != 0 || i == youtubeUnique.length-1){
						strCount = 0;
						strArr.push(apiStr);
						apiStr = '';
					}
				}

				var apiUrl = "";
					for(var i=0;i<strArr.length;i++) {
						apiUrl = "http://gdata.youtube.com/feeds/api/videos?q=" + strArr[i] + "&alt=json";

						$.getJSON(apiUrl, function(data) {
					 	var items = [];

					  	result = data.feed.entry;

					  	//video object toevoegen aan video array
						for(var j=0;j<result.length;j++){
							var obj = {};
							var id = (result[j].id.$t).split('/');
							id = id[id.length-1];
							obj.id = id;
							obj.img = 'http://i4.ytimg.com/vi/' + id + '/mqdefault.jpg';
							obj.title = result[j].title.$t;
							obj.client = 'y';
							obj.author = result[j].author[0].name.$t;
							videoArr.push(obj);

							if(videoArr.length >= youtubeUnique.length){
								videosLoaded();
							}
						}
					});
				}
			}

		break;
		/* YOUTUBE END */

		/* VIMEO */
		case 'v':
			var linkArr = new Array();
			var videoUrl = url.replace(/[^0-9]/g, '');
		 	if(videoUrl.length >= 6){
				var apiUrl = "//vimeo.com/api/v2/video/" + videoUrl + ".json";	
				linkArr.push(apiUrl);
		 	} else {
				$('a').each(function(){
					if($(this).attr('href') !== undefined){
						url = $(this).attr('href').replace('/','');
						if(!isNaN(url) && url != ''){
							var apiUrl = "//vimeo.com/api/v2/video/" + url + ".json";
							linkArr.push(apiUrl);
						}
					}
				});
		 	}

		 	for(var i=0; i<linkArr.length;i++){
				vimeoRequest(linkArr[i], linkArr.length);
				console.log('request: ' + linkArr[i]);
		 	}
			
		break;
		/* VIMEO END */
	}

	//en alles mooi terug returnen
	return videoArr;
}

function vimeoRequest(url, total){
	$.getJSON(url, function(data) {
		var video = data[0];
		var obj = {};
		obj.id = video.id;
		obj.img = video.thumbnail_large;
		obj.title = video.title
		obj.client = 'v';
		obj.author = video.user_name;
		videoArr.push(obj);
		if(videoArr.length == total){
			videosLoaded();
		}
	});
}

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}


/*TITLES TE CLEANEN (zit soms clutter tussen zoals enters en whitespaces etc)*/
function cleanTitle(str) {
	//enters weg doen
	str = str.replace(/(\r\n|\n|\r)/gm,"");
	//whitespace in begin en einde weg doen
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}