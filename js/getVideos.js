/**
*
* Dit is voor alle videos af te halen ven verschillende clients (youtube/vimeo/...)
* Staat in een aparte file omdat dit ook gebruikt wordt voor de Chrome extentie
* Hierdoor worden aanpassingen ineens naar bookmarklet en extentie gepushed
*
**/

var totalVideos = 0;

function getVideos(client){
	//variables
	var url = document.URL;
	var videoArr = new Array();

	//switch voor de client (host/provider/..) als er een nieuwe client supported is moet er gewoon een nieuwe case toegevoegt worden
	switch(client){
		/* DEFAULT (alles ophalen van iframes enzo) */
		default:
			var vimeoArr =  new Array();
			var youtubeArr = new Array();

			//alles op google + zoeken a.d.h.v. ot-anchor
			$('.ot-anchor').each(function(){
				var link = $(this).attr('href');
				if(link.indexOf('youtu') >= 0){

					var splitUrl = link.split('/');
					link = splitUrl[splitUrl.length-1];
					youtubeArr.push(link);

					if(params.v !== undefined){
						youtubeArr.push(params.v);
					}
				}
				
				if(link.indexOf('vimeo') >= 0){
					var splitUrl = link.split('/');
					link = splitUrl[splitUrl.length-1];
					if(!isNaN(link) && link != ''){
						vimeoArr.push(link);
					}
				}
			});

			//elke url op de pagina afgaan en de href opvragen
			$('a').each(function(){
				var link = $(this).attr('href');

				//check for youtube videos
				if(link.indexOf('youtube') >= 0){
					//video paramater opzoeken (videoID)
					var link=link.replace("/watch","");
					var params = getQueryParams(link);
					if(params.v !== undefined){
						youtubeArr.push(params.v);
					}
				}
				
				//check for vimeo videos
				if(link.indexOf('vimeo') >= 0){
					var splitUrl = link.split('/');
					link = splitUrl[splitUrl.length-1];
					if(!isNaN(link) && link != ''){
						vimeoArr.push(link);
					}
				}
			});

			//als er iframes gevonden zijn
			$('iframe').each(function(){
				//src van iframe ophalen en het woord youtube of vimeo er uit halen
				var src = $(this).attr('src');
				//if youtube
				if(src.indexOf('youtube') >= 0){
					//video ID uit src halen
					var splitUrl = src.split('/');
					if(splitUrl[splitUrl.length-1].indexOf('?') >= 0){
						splitUrl = splitUrl[splitUrl.length-2];
					}else{
						splitUrl = splitUrl[splitUrl.length-1];
					}
					youtubeArr.push(splitUrl);
				}
				//if vimeo
				else if(src.indexOf('vimeo') >= 0){
					//video ID uit src halen
					var splitUrl = src.split('/');
					vimeoArr.push(splitUrl[splitUrl.length-1]);
				}
			});


			//alle duplicates deleten
			var youtubeUnique = new Array(); 
			$.each(youtubeArr, function(i, el){
			    if($.inArray(el, youtubeUnique) === -1) youtubeUnique.push(el);
			});
			var vimeoUnique = new Array(); 
			$.each(vimeoArr, function(i, el){
			    if($.inArray(el, vimeoUnique) === -1) vimeoUnique.push(el);
			});

			//alle info requesten
			totalVideos = youtubeUnique.length + vimeoUnique.length;
			youtubeRequest(youtubeUnique);
			vimeoRequest(vimeoUnique);
		break;
		/* DEFAULT END */

		/* YOUTUBE */
		case 'y':
			
			//video ID uit url halen
			splitUrl = url.split('v=');
			if(splitUrl[1] !== undefined){
				url = splitUrl[1].split('&');
				var cleanUrl = url[0].replace(/\b\#+/g, '');
				cleanUrl = cleanUrl.replace(/\b\!+/g, '');
			}

			if(cleanUrl){
				var youtubeArr = new Array(cleanUrl);
				totalVideos = youtubeArr.length;
				youtubeRequest(youtubeArr);
			} else {
				var youtubeArr = new Array();

				//elke a tag opzoeken en href opvragen
				$('a').each(function(){
					var link = $(this).attr('href')

					var patt1=new RegExp("watch");
					//checken of er watch in de url staat
					if(patt1.test(link)){

						//video paramater opzoeken (videoID)
						var link=link.replace("/watch","");
						var params = getQueryParams(link);
						if(params.v !== undefined){
							youtubeArr.push(params.v);
						}
					}
				});
				totalVideos = youtubeArr.length;
				youtubeRequest(youtubeArr);
			}

		break;
		/* YOUTUBE END */

		/* VIMEO */
		case 'v':
			var linkArr = new Array();
			var videoUrl = url.replace(/[^0-9]/g, '');
		 	if(videoUrl.length >= 6){
				var apiUrl = "//vimeo.com/api/v2/video/" + videoUrl + ".json";	
				linkArr.push(apiUrl);
		 	} else {
				$('a').each(function(){
					if($(this).attr('href') !== undefined){
						url = $(this).attr('href').replace('/','');
						if(!isNaN(url) && url != ''){
							//var apiUrl = "//vimeo.com/api/v2/video/" + url + ".json";
							linkArr.push(url);
						}
					}
				});
		 	}

			totalVideos = linkArr.length;
		 	vimeoRequest(linkArr);
			
		break;
		/* VIMEO END */
	}

	//en alles mooi terug returnen
	return videoArr;
}

function youtubeRequest(youtubeArr){
	var youtubeUnique = new Array();
	//duplicates deleten
	$.each(youtubeArr, function(i, el){
	    if($.inArray(el, youtubeUnique) === -1) youtubeUnique.push(el);
	});

	//per 10 rekenen
	var apiStr = "";
	var strArr = new Array();
	var strCount = 0;
	
	totalVideos = youtubeUnique.length;

	//per 10 videos in een array doen
	//als er een - in voor komt moet die ID in een andere item
	if(youtubeUnique.length != 1){
		for(var i=0;i<youtubeUnique.length;i++) {
			if(youtubeUnique[i].indexOf("-") == -1){
				apiStr += youtubeUnique[i] +'|';
				strCount ++;
			} else {
				if(apiStr != ''){
					strArr.push(apiStr);
				}
				strArr.push(youtubeUnique[i]);
				apiStr = '';
				strCount = 1;
			}
			if(strCount % 10 === 0 && i != 0 || i == youtubeUnique.length-1){
				strCount = 0;
				strArr.push(apiStr);
				apiStr = '';
			}
		}
	} else {
		strArr.push(youtubeUnique[0]);
	}

	//voor elke reeks videos een call doen
	var apiUrl = "";
	for(var i=0;i<strArr.length;i++) {
		apiUrl = "http://gdata.youtube.com/feeds/api/videos?q=" + strArr[i] + "&alt=json&fields=entry(id,title,author)";

		$.getJSON(apiUrl, function(data) {

	  		var result = data.feed.entry;
	  		if(result === undefined){
	  			videosLoaded();
	  		}

		  	//video object toevoegen aan video array
			for(var j=0;j<result.length;j++){
				var obj = {};
				var id = (result[j].id.$t).split('/');
				id = id[id.length-1];
				obj.id = id;
				obj.img = 'http://i4.ytimg.com/vi/' + id + '/mqdefault.jpg';
				obj.title = result[j].title.$t;
				obj.client = 'y';
				obj.author = result[j].author[0].name.$t;
				videoArr.push(obj);
				
				checkLoaded();
			}
		});
	}
}

function vimeoRequest(vimeoArr){
	for(var i=0;i<vimeoArr.length;i++) {
		var apiUrl = "//vimeo.com/api/v2/video/" + vimeoArr[i] + ".json";
		$.getJSON(apiUrl, function(data) {
		var video = data[0];
			var obj = {};
			obj.id = video.id;
			obj.img = video.thumbnail_large;
			obj.title = video.title
			obj.client = 'v';
			obj.author = video.user_name;
			videoArr.push(obj);

			checkLoaded();
		});
	}
}

function checkLoaded(){
	newVideo(videoArr.length);
	/*if(videoArr.length == totalVideos){
		videosLoaded();
	}*/
}

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}


/*TITLES TE CLEANEN (zit soms clutter tussen zoals enters en whitespaces etc)*/
function cleanTitle(str) {
	//enters weg doen
	str = str.replace(/(\r\n|\n|\r)/gm,"");
	//whitespace in begin en einde weg doen
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}