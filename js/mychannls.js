//global variables
var myChanllUrl = "https://www.mychannls.com/bookmarklet/";
var client = "";
var videoArr = new Array();

/*jquery inladen tenzij het al ingeladen is*/
if (!($ = window.jQuery)){
	script = document.createElement('script');
   	script.src = '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'; 
	script.onload=loadScripts;
	document.body.appendChild(script);
} 
else {
	loadScripts();
}

/*<script type="text/javascript" src="//use.typekit.net/dre3arw.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>*/

//als jquery ingeladen is getVideos en typekit ook inladen (voor de standalone functie om alle videos op te halen)
function loadScripts(){
	script = document.createElement('script');
   	script.src = myChanllUrl + 'js/getVideos.js';
	script.onload=loadCSS;
	document.body.appendChild(script);

	/*typekit = document.createElement('script');
   	typekit.src = myChanllUrl + '//use.typekit.net/dre3arw.js';
	typekit.onload=loadCSS;
	typekit.onload=loadTypekit;
	document.body.appendChild(script);*/
}

//vanaf typekit script ingeladen is typekit starten
function loadTypekit(){
	try{Typekit.load();}catch(e){}
}

//als getVideos ingeladen is css ook inladen
function loadCSS(){
	var style = document.createElement('link');
	style.setAttribute('rel', 'stylesheet');
	style.setAttribute('type', 'text/css');
	style.setAttribute('href',  myChanllUrl + 'css/style.css');
	style.onload=init;
	document.getElementsByTagName('head')[0].appendChild(style);
}

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};

BrowserDetect.init();


//als alle script ingeladen zijn beginnen met op te bouwen
function init() {

	console.log('init');

	//video wmode transparant setten (om alles naar achter te zetten)
	//$("#movie_player").attr('wmode', 'transparent');
	if(BrowserDetect.browser != 'Chrome'){
		$("#movie_player").hide();
	}

	//naar de top scrollen
	if($(window).scrollTop() > 0){
		$('body,html').animate({
			scrollTop: 0
		}, 800);
	}
	
	//client ophalen
	client = checkClient();

	//als het youtube of vimeo is: filmpjes ophalen
	videoArr = getVideos(client);

	//checken of overlay al geadd is (indien al geadd niet meer adden = voor mensen die de bookmarklet spammen met clicks)
	//overlay is nu niet echt uniek dus hier moet nog een andere oplossing voor gevonden worden
	if($('#overlay').length == 0){

		//overlay adden
		$('body').append('<div id="overlay"></div>');
		//content aan overlayContent toevoegen (niet aan overlay want dan zou daar ook transparancy op worden toegepast)
		$('body').append('<div id="overlayContent"></div>');
		//de topbar toevoegen
		$('#overlayContent').append('<div id="topbar"><img src="' + myChanllUrl + 'img/logo.png" /><p class="close">X</p><div class="clearingDiv">&nbsp;</div></div>');

		//animeren on buildup
		$('#overlay, #overlayContent').css({opacity: 0});
		$('#overlay').animate({opacity: 0.7});
		$('#overlayContent').animate({opacity: 1})

		/*checken op welke ID er geklikt is om dan het scherm te clearen*/
		$('#overlayContent').click(function(){
			//event.target.id moet opgehaald worden anders wordt het scherm ook gecleared als er op de thumbs gedrukt wordt
			if(event.target.id == 'overlayContent' || event.target.id == "topbar" || event.target.id == "btnClose"){
				clearScreen();
			}
		});

		//scherm clearen als het gesloten wordt
		$(".close, #overlay").click(function(){
			clearScreen();
		});
	}

	/** LAYOUT **/
    //layout callen on init
    layout();

	//on resize -> functie layout uitvoeren en css toepassen
    $(window).resize(function(){
        layout();
    });
	/**- LAYOUT -**/
}



//listener voor als er een nieuwe videos geladen is
function newVideo(count){
	var i = count-1;
	//frame toevoegen
	var click = "'http://alpha.mychannls.com/bookmarklet/" + videoArr[i].client + "/" + videoArr[i].id + "','bookmarket'";
	$('#overlayContent').append('<div class="videoFrame"  onclick="NewWindow(' + click + ')" id="' + videoArr[i].id + '"><div class="videoOverlay"></div><img class="addVideo" src="//clients.contentcowboys.eu/mychannls/img/addvideo.png" /></div>');

	//titel toevoegen (en author indien gevonden)
	if(videoArr[i].author != undefined && videoArr[i].author != "Unknown"){
		$('#'+videoArr[i].id).append('<p class="videoTitle">' + videoArr[i].title + '<p class="by">by <span class="name">' + videoArr[i].author + '</span></p>' +'</p>');
	} else {
		$('#'+videoArr[i].id).append('<p class="videoTitlea">' + videoArr[i].title + '</p>');
	}
	//background changen
	$('#'+videoArr[i].id).css({'background-image':'url("'+ videoArr[i].img + '")'});

	layout();
}


/* CHECK CLIENT */
function checkClient(){
	switch(document.domain){
		case 'www.youtube.com':
			return 'y';
		break;
		case 'vimeo.com':
			return 'v';
		break;
		default:
			return '';
		break;
	}
}
/*- CHECK CLIENT -*/


/* CLEAR SCREAN */
function clearScreen(){
	$('#overlay, #overlayContent').animate({opacity: 0},function(){
		$('#overlayContent, #overlay').remove();
	});
	$("#movie_player").show();
}
/*-CLEAR SCREAN-*/


/* LAYOUT */
function layout(){
	//als er maar 1 video is de breedte groter maken
	if($('.videoFrame').length == 1){

		$('.videoFrame').attr('class', 'videoFrame single');
		/*$('.videoFrame').width(424);
		$('.videoFrame').height(220);
		$('.videoFrame .videoTitle').css({'font-size':'48px', 'line-height':'48px','width':'auto','bottom':'45px'})
		$('.videoFrame .by').css({'font-size':'20px','bottom':'15px'});
		$('.videoFrame .by span').css({'font-size':'28px'});*/
	} else {
		$('.videoFrame').attr('class', 'videoFrame multiple');
	}

	//breedte berekenen van overlaycontent adhv hoeveel filmpjes er staan
	if(videoArr.length != 0){
		var vidW = $('.videoFrame').width() + parseInt($('.videoFrame').css('margin').replace("px", ""))*2 + parseInt($('.videoFrame').css('padding').replace("px", ""))*2;
	}
	if(videoArr.length == 1){
		$('#overlayContent').css({'position':'absolute','margin':'50px','width': vidW});
	} else{
		var contW = 0;
		while(contW <= $(window).width()){
			contW = contW + vidW;
		}
		$('#overlayContent').css({'position':'absolute','margin':'50px','width':contW-vidW});
	}


	//videos centreren
	if(videoArr.length == 1){
		$('#overlayContent').css({'left': ($(window).width()-$('#overlayContent').width())/2-50,'top':'100px'});
	} else{
		$('#overlayContent').css({'left': ($(window).width()-$('#overlayContent').width())/2-50});
	}

	//video naar achtergrond brengen in firefox
	if(navigator.userAgent.indexOf('Firefox') != -1){
		$('#movie_player').css({'z-index':-1});
	}

	//checken of de titel langer is als 3 regels (= cappen op 3 regels)
	$(".videoFrame .videoTitle").each(function( index ) {
		if(videoArr.length > 1){
			//enkel als er meerdere videos zijn
			if($(this).height() > 72){
				$(this).height(72);
			}
		} else {
			//enkel als er maar 1 video is
			if($(this).height() > 200){
				$(this).height(200);
			}
		}
	});

	//no video layout
	$('#noVideo').css({'top': ($(window).height()-$('#noVideo').height())/2-50});
}
/*- LAYOUT END -*/


/* Openen van een popup voor adden van video */
function NewWindow(mypage,myname){
	clearScreen();

    settings="width=740,height=410,top=100,left=200,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
    win=window.open(mypage,myname,settings);
    win.focus();
}